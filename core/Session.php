<?php

class Session
{
	public static $isSessionStarted = false;
	public static function sessionStart()
	{
		if(!Session::$isSessionStarted) {
			session_start();
			Session::$isSessionStarted = true;
		}
	}

	public static function sessionSet($key, $value)
	{
		if(Session::$isSessionStarted) {
			$_SESSION[$key] = $value;
		}
	}

	public static function sessionGet($key)
	{
		if(Session::$isSessionStarted) {
			if(!empty($_SESSION)) {
				return $_SESSION[$key];
			} else {
				return NULL;
			}
			
		} else {
			return NULL;
		}
	}

	public static function sessionClose(){
		if(Session::$isSessionStarted){
			Session::$isSessionStarted = false;
			session_destroy();
		}
	}

}
?>