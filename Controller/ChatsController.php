<?php
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/core/Session.php');
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/core/Controller.php');

require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/Model/User.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/Model/Conversation.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/Model/Chat.php');

class ChatsController extends Controller {
	public function __construct() {
	}

	public function index() {
		$user = new User();
		Session::sessionStart();
		$user->find(Session::sessionGet('id'));
		$conversations = $user->conversation();
		$results = array();
		foreach ($conversations as $conversation) {
			array_push($results, $conversation->get());
		}
		return json_encode($results);
	}

	public function index_users($data) {
		$conversation = new Conversation();
		//die(json_encode($data));
		$conversation->find($data['conversation_id']);
		$users = $conversation->user();
		$results = array();
		foreach ($users as $user) {
			array_push($results, $user->get());
		}
		return json_encode($results);
	}

	public function storeChat($data) {
		$chat = new Chat();
		$data['user_id'] = $this->current_user_id();
		$chat->create($data);
		$chat->save();
		return $this->show($data);
	}

	public function storeConversation($data) {
		$conversation = new Conversation();
		$conversation->create($data);
		$conversation->save();
		$conversation = $conversation->where(['name' => $data['name']]);
        $conversation = end($conversation)->get();
        Session::sessionStart();
        $this->joinConversation(array('conversation_id' => $conversation['id'] , "user_id" => Session::sessionGet('id') ));
	}

	public function show($data) {
		$conversation = new Conversation();
		$conversation->find($data['conversation_id']);
		$chats = $conversation->chat();
		$fresults = array();
		$fresults['conv_info'] = $conversation->get();
		$results = array();
		foreach ($chats as $chat) {
			$result['chat'] = $chat->get();
			$result['user'] = array('fname' => $chat->user()->get()['fname'],
				'lname' => $chat->user()->get()['lname']);
			array_push($results, $result);
		}
		$fresults['chats'] = $results;
		return json_encode($fresults);
	}

	public function joinConversation($data) {
		$conversation = new Conversation();
		$conversation->find($data['conversation_id']);
		$conversation->many_to_many_attach('user',['user_id' => $data['user_id'] , 'conversation_id' => $data['conversation_id'] ]);
		return $this->show(array('conversation_id' => $data['conversation_id']));	
	}

	private function current_user_id() {
		Session::sessionStart();
		return Session::sessionGet('id');
	}

}
?>