<?php

Class Commands{


	// protected db_connection;
	//connect to database
	public function db_connect($db_host, $db_user, $db_name, $db_password){
		$this->db_connection = mysqli_connect($db_host, $db_user, $db_name, $db_password);
		echo $this->db_connection->error;
		if(! $this->db_connection){
			die('couldn\'t connect:' . mysqli_error() );
		}
		return $this->db_connection;
	}

	/*------------------------------------------------------------------------------------------------------------------------------*/

	//close database connection
	public function db_close_connection(){
		mysql_close($this->db_connection);
	}

	/*------------------------------------------------------------------------------------------------------------------------------*/

	//create to table
	public function table_creation($schema_path){
		$fp = fopen($schema_path, 'r');
		$db_schema = fread($fp, filesize($schema_path));
		fclose($fp);
		// die($db_schema);
		$tables_connect = $this->db_connection->query($db_schema);
		if($this->db_connection->error){
	        die('Couldn\'t create table: ' . $this->db_connection->error);
	    }
	    return true;
	}

	/*------------------------------------------------------------------------------------------------------------------------------*/
	//insert into database
	public function db_insert($table_name, $fields, $values){
		$this->db_connection->query("INSERT INTO $table_name ($fields) VALUES ($values)");
		return $this->db_connection->insert_id;
		// die($this->db_connection->error);
	}

	public function db_insert_attach($table_name, $fValue, $lValue){
		$this->db_connection->query("INSERT INTO $table_name VALUES ($fValue, $lValue)");
	}

	public function db_insert_attach_many($table_name, $data){
		$this->db_connection->query("INSERT INTO $table_name $data");
	}
	

	/*------------------------------------------------------------------------------------------------------------------------------*/
	//select value from database
	public function db_select($table_name, $field, $condition){
		return mysqli_fetch_assoc($this->db_connection->query("SELECT ($field) FROM `$table_name` $condition "));
	}

	public function db_select_all_by_id($table_name, $id){
		return mysqli_fetch_assoc($this->db_connection->query("SELECT * FROM `$table_name` WHERE id = $id "));
	}

	public function db_select_all_by_id_belongs($ftable, $stable, $id){
		$string = rtrim(explode("_", $stable)[1], "s");
		return ($this->db_connection->query("SELECT * FROM $ftable WHERE $ftable.id IN (SELECT $string"."_id FROM $ftable NATURAL JOIN $stable WHERE $ftable.id = $id)"));
	}

	public function db_select_all_by_id_belongs_many($ftable, $stable, $id){
		// die($ftable." : ".$stable);
		$string = rtrim(explode("_", $ftable)[0], "s");
		$query_conditions = explode("_", $stable);
		if($query_conditions[0] == $ftable) {
			$query_conditions = rtrim($query_conditions[1], 's');
		} else {
			$query_conditions = rtrim($query_conditions[0], 's');
		}
		$query = ("SELECT * FROM $ftable WHERE $ftable.id IN (SELECT $string"."_id FROM $ftable NATURAL JOIN $stable WHERE $query_conditions"."_id = $id)");
		return ($this->db_connection->query($query));
	}

	public function db_select_all_by_id_many($table_name, $key, $value){
		return $this->db_connection->query("SELECT * FROM `$table_name` WHERE $key = $value ");
	}

	public function db_select_all_where($table_name, $condition){
		// die("SELECT * FROM `$table_name` WHERE $condition");
		return $this->db_connection->query("SELECT * FROM `$table_name` WHERE $condition");
	}
	public function db_select_all_table($table_name){
		// die("SELECT * FROM `$table_name` WHERE $condition");
		return $this->db_connection->query("SELECT * FROM `$table_name`");
	}

	/*public function db_select($db_name, $field, $groupBy, $having){
		return mysqli_fetch_assoc($this->db_connection->query("SELECT ($field) FROM `$db_name` GROUP BY $groupBy HAVING $having"));
	}*/
	/*------------------------------------------------------------------------------------------------------------------------------*/
	//update column 
	public function db_update($table_name, $field, $condition){
		// die("UPDATE `$table_name` SET $field WHERE id = $condition");
		$this->db_connection->query("UPDATE `$table_name` SET $field WHERE id = $condition");
	}
	/*------------------------------------------------------------------------------------------------------------------------------*/
	//delete row
	public function db_delete($table_name, $key){
		$this->db_connection->query("DELETE FROM `$table_name` WHERE id = $key");
	} 
	public function db_delete_dettach($table_name, $key, $role, $value){
		// die("DELETE FROM `$table_name` WHERE id = $key AND $role'.'_id = $value");
		$this->db_connection->query("DELETE FROM `$table_name` WHERE id = $key AND $role"."_id = $value");
	} 
}

// $test = new Commands();
// $test->db_connect('localhost', 'root', 'ahmedmagdy711', 'facebook');
// echo var_dump(get_class($this));
// $test->table_creation('/var/www/html/facebook/socialmedia/database/Schema.sql');
// $test->db_insert('countries','`id`, `country_code`, `country_name`' , 'null, \'AF\', \'Afghanistan\'');
?>