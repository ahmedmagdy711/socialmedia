
CREATE TABLE IF NOT EXISTS `posts` (
	`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`group_id` INT ,
	`body` VARCHAR(255) NOT NULL,
	`img` VARCHAR(255),
	`public` VARCHAR(50) DEFAULT 'private',
	`date` TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (group_id)REFERENCES groups(id) ON DELETE CASCADE
);