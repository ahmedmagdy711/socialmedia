CREATE TABLE IF NOT EXISTS `chats` (
	`id` INT PRIMARY KEY  AUTO_INCREMENT,
	`conversation_id` INT NOT NULL,
	`user_id` INT NOT NULL,
	`chat_text` VARCHAR(10000),
	`chat_time` TIMESTAMP,
	FOREIGN KEY (conversation_id) REFERENCES conversations(id) ON DELETE CASCADE,
	FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);