drop database facebook;

create database facebook;

use facebook;


CREATE TABLE IF NOT EXISTS `users` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`fname` VARCHAR(255) NOT NULL,
	`lname` VARCHAR(255) NOT NULL,
	`password` VARCHAR(50) NOT NULL,
	`email` VARCHAR(255) UNIQUE NOT NULL,
	`telephone` VARCHAR(255) ,
	`gender` VARCHAR(10) NOT NULL,
	`birthdate` DATE NOT NULL,
	`profile_image` VARCHAR(255) DEFAULT '/img/pp.jpg',
	`hometown` VARCHAR(10), 
	`marital_status` VARCHAR(255),
	`about` VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS `groups` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(255) UNIQUE,
	`date` TIMESTAMP
);


CREATE TABLE IF NOT EXISTS `posts` (
	`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`group_id` INT ,
	`body` VARCHAR(255) NOT NULL,
	`img` VARCHAR(255),
	`public` VARCHAR(50) DEFAULT 'private',
	`date` TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (group_id)REFERENCES groups(id) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS `comments` (
	`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`user_id` INT NOT NULL,
	`post_id` INT NOT NULL,
	`body` VARCHAR(255) NOT NULL,
	`date` TIMESTAMP,
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (post_id) REFERENCES posts (id)
);


CREATE TABLE IF NOT EXISTS `likes` (
	`post_id` INT NOT NULL ,
	`user_id` INT NOT NULL,
	FOREIGN KEY (post_id) REFERENCES posts (id),
	FOREIGN KEY (user_id) REFERENCES users (id),
	PRIMARY KEY (post_id, user_id)
);



CREATE TABLE IF NOT EXISTS `users_friends` (
	`id` INT NOT NULL ,
	`friend_id` INT NOT NULL,
	FOREIGN KEY (id) REFERENCES users (id),
	FOREIGN KEY (friend_id) REFERENCES users (id),
	PRIMARY KEY (id, friend_id)
);

CREATE TABLE IF NOT EXISTS `notifications`(
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`body` VARCHAR(200) NOT NULL,
	`group_id` INT,
	`date` TIMESTAMP,
	FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE 
);


CREATE TABLE IF NOT EXISTS `users_requests` (
	`id` INT NOT NULL ,
	`request_id` INT NOT NULL,
	FOREIGN KEY (id) REFERENCES users (id),
	FOREIGN KEY (request_id) REFERENCES users (id),
	PRIMARY KEY (id, request_id)
);


CREATE TABLE IF NOT EXISTS `conversations`(
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(50),
	`date` TIMESTAMP
);


CREATE TABLE IF NOT EXISTS `chats` (
	`id` INT PRIMARY KEY  AUTO_INCREMENT,
	`conversation_id` INT NOT NULL,
	`user_id` INT NOT NULL,
	`chat_text` VARCHAR(10000),
	`chat_time` TIMESTAMP,
	FOREIGN KEY (conversation_id) REFERENCES conversations(id) ON DELETE CASCADE,
	FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `users_conversations`(
	`user_id` INT,
	`conversation_id` INT,
	PRIMARY KEY(user_id, conversation_id),
	FOREIGN KEY (user_id) REFERENCES users(id),
	FOREIGN KEY (conversation_id) REFERENCES conversations(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `users_groups` (
	`user_id` INT,
	`group_id` INT,
	`join_Date` TIMESTAMP,
	PRIMARY KEY (user_id, group_id),
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `users_notifications`(
	`notification_id` INT,
	`user_id` INT,
	PRIMARY KEY(notification_id, user_id),
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (notification_id) REFERENCES notifications (id) ON DELETE CASCADE
);

