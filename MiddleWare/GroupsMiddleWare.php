<?php
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Session.php');

class Groups {
	private $methodName, $controllerfilePath;
	private $dataErrors = array();
	public $access;

	public function __construct($controllerfilePath, $methodName) {
		$this->methodName = $methodName;
		$this->controllerfilePath = $controllerfilePath;
		$this->access = True;
		Session::sessionStart();
	    if(Session::sessionGet('id') == NULL){
			$this->access = False;
		}
	}

	public function getAccess() {
		return $this->access;
	}


}
?>