CREATE TABLE IF NOT EXISTS `users_conversations`(
	`user_id` INT,
	`conversation_id` INT,
	PRIMARY KEY(user_id, conversation_id),
	FOREIGN KEY (user_id) REFERENCES users(id),
	FOREIGN KEY (conversation_id) REFERENCES conversations(id) ON DELETE CASCADE
);