<?php
require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Controller.php';
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Session.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/User.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/Group.php');

class PagesController extends Controller
{
	private $modelClass;
	public function __construct()
	{
		// die("");
		// $modelClass = Parent::$Model;
	}
	//register
	public function getHome() {
		Session::sessionStart();
		if(Session::sessionGet('id') == NULL) {
			return $this->loadView('mainPageNotLoged');
		} else {
			return $this->loadView('mainPageLoged');
		}
	}

	public function search($data) {
		if($data['query'] == "") return json_encode(array());
		$user = new User();
		$users = $user->all();
		$usersResult = array();
		$result = array();
		foreach ($users as $ssuser) {
			$suser = $ssuser->get();
			if($suser['id'] == $this->current_user_id())
				continue;
			unset($suser['password']);
			unset($suser['access_token']);
			$query = strtolower($data['query']);
			$query = "/$query/";
			if(preg_match("$query",strtolower($suser['fname']))
			 || preg_match("$query",strtolower($suser['lname']))
			 || preg_match("$query",strtolower($suser['telephone']))
			 || preg_match("$query",strtolower($suser['hometown']))
			 || preg_match("$query",strtolower($suser['email'])) )
				array_push($usersResult, $suser);

			$posts = $ssuser->post();
			foreach ($posts as $post) {
				if(preg_match("$query",strtolower($post->get()['body']))) {
					$tuser = $post->user()->get();
					unset($tuser['password']);
					$inlist = false;
					foreach ($usersResult as $sresult) {
						if($sresult['id'] == $tuser['id'])
							$inlist = true;
					}
					if(!$inlist)
						array_push($usersResult, $tuser);
				}
			}
			// array_unique($usersResult);
		}
		$result['user'] = $usersResult;

		$group = new Group();
		$groups = $group->all();
		$groupsResult = array();
		foreach ($groups as $sgroup) {
			$sgroup = $sgroup->get();
			unset($sgroup['password']);
			unset($sgroup['access_token']);
			$query = strtolower($data['query']);
			$query = "/$query/";
			if(preg_match("$query",strtolower($sgroup['name'])) )
				array_push($groupsResult, $sgroup);
		}
		$result['group'] = $groupsResult;
		return json_encode($result);
		// if (strpos($a,'are') !== false) {
		//     echo 'true';
		// }
	}

	private function current_user_id() {
		Session::sessionStart();
		return Session::sessionGet('id');
	}
}
?>