<?php 
class routes {
	function getroutes() {
		return array(
		"GET" => array(
			"/" => "Pages:getHome",
			"/search" => "Pages:search",

			"/auth/login" => "Auth:getLogin",
			"/auth/register" => "Auth:getRegister",
			"/auth/login" => "Auth:getLogin",
			"/auth/logout" => "Auth:getLogout",
			"/auth/passReset" => "Auth:getPassReset",

			"/posts/index" => "Posts:index",
			"/posts/userpost" => "Posts:userpost",
			"/posts/create" => "Posts:create",
			"/posts/show" => "Posts:show",
			"/posts/edit" => "Posts:edit",
			"/posts/like" => "Posts:like",

			"/groups/index" => "Groups:index",
			"/groups/create" => "Groups:create",
			"/groups/show" => "Groups:show",
			"/groups/insert" => "Groups:add",

			"/users/show" => "Users:show",
			"/users/pp" => "Users:profilepic",
			"/users/me" => "Users:me",
			"/users/add" => "Users:add",
			"/users/accept" => "Users:accept",
			"/users/requests" => "Users:request",
			"/users/friends" => "Users:friends",
			"/users/update" => "Users:update",

			"/notifications/index" => "Notifications:index",

			"/chat/storeConversation" => "Chats:storeConversation",
			"/chat/storeChat" => "Chats:storeChat",
			"/chat/index" => "Chats:index",
			"/chat/show"=>"Chats:show",
			"/chat/joinConversation"=>"Chats:joinConversation",
			"/chat/indexUsers" => "Chats:index_users"

			),
		"POST" => array(
			"/auth/register" => "Auth:postRegister",
			"/auth/login" => "Auth:postLogin",
			"/auth/passReset" => "Auth:postPassReset",

			"/posts/create" => "Posts:store",
			"/posts/edit" => "Posts:update",
			"/posts/comment" => "Posts:comment",

			"/groups/store" => "Groups:store",
			"/groups/insert" => "Groups:insert"
			)
		);
	}
}
?>