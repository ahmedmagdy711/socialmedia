<?php
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/core/Session.php');
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/core/Controller.php');

require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/Model/User.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/Model/Group.php');

class GroupsController extends Controller {
	public function __construct() {
	}

	public function index() {
		$user = new User();
		Session::sessionStart();
		$user->find(Session::sessionGet('id'));
		$groups = $user->group();
		$result = array();
		foreach ($groups as $group) {
			array_push($result, $group->get());
		}
		return json_encode($result);
	}

	public function create() {
	}

	public function store($data) {
		$group = new Group();
		$group->create($data);
		$group->save();
		$group = $group->where(['name' => $data['name']])[0];
		Session::sessionStart();
		$group->many_to_many_attach('User', ['user_id' => Session::sessionGet('id'),
			'group_id' => $group->get()['id']]);
		return $this->index();
	}

	public function show($data) {

		$group = new Group();
		$group->find($data['id']);
		$posts =$group->post();
		$group = $group->get();

		$results = array();
		foreach ($posts as $post) {
			$result = $post->get();
			$result['user'] = array('fname' => $post->user()->get()['fname'],
				'lname' => $post->user()->get()['lname']);
			$comments = array();
			for ($i = 0 ; $i < count($post->comment()) ; $i++) {
				$comments[$i] =  $post->comment()[$i]->get();
			}
			$result['comments'] = $comments;
			$likes = array();
			for ($i = 0 ; $i < count($post->like()) ; $i++) {
				$user = new User();
				$user->find($post->like()[$i]['user_id']);
				$likes[$i] =  ['id' => $user->get()['id'] , 'name' => $user->get()['fname']] ;
			}
			$result['likes'] = $likes;
			array_push($results, $result);
		}
		 	return(json_encode($results));
	}

	public function add() {
	}

	public function insert ($data) {
		$group = new Group();
		$group->find($data['group_id']);
		$group->many_to_many_attach('user',['user_id' => $data['user_id'] , 'group_id' => $data['group_id'] ]);
		return $this->show(array('id' => $data['group_id']));
	}	
}
?>