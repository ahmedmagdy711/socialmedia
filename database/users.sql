
CREATE TABLE IF NOT EXISTS `users` (
	`id` INT PRIMARY KEY AUTO_INCREMENT,
	`fname` VARCHAR(255) NOT NULL,
	`lname` VARCHAR(255) NOT NULL,
	`password` VARCHAR(50) NOT NULL,
	`email` VARCHAR(255) UNIQUE NOT NULL,
	`telephone` VARCHAR(255) ,
	`gender` VARCHAR(1) NOT NULL,
	`birthdate` DATE NOT NULL,
	`profile_image` VARCHAR(255) DEFAULT '/img/pp.jpg',
	`hometown` VARCHAR(10), 
	`marital_status` VARCHAR(255),
	`about` VARCHAR(255) NOT NULL
);