<?php
require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Controller.php';
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Session.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/User.php');

class AuthController extends Controller
{
	private $modelClass;
	public function __construct()
	{
		// die("");
		// $modelClass = Parent::$Model;
	}
	//register
	public function getRegister() {
		return $this->loadView('register');
	}

	public function postRegister($data) {
			$iserror = false;
			$errorarray = array();
			if($data['password'].length < 6) {
				$iserror = true;
				array_push($errorarray, 'invalid Password');
			}
			if(!preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i',
			 $data['email'])){
				$iserror = true;
				array_push($errorarray, 'invalid email');
			}
			if($data['fname'][0]== " " || $data['fname'][0] =="" ||
				$data['lname'][0]== " " || $data['lname'][0] =="") {
				$iserror = true;
				array_push($errorarray, 'invalid name');
			}
			if($data['birthdate'][0]== " " || $data['birthdate'][0] =="" ) {
				$iserror = true;
				array_push($errorarray, 'invalid birthdate');
			}
			$data['password'] = md5($data['password']);
			if($iserror) {
				return json_encode($errorarray);
			}
			$user = new User();
			$user->create($data);
			$user->save();
			Session::sessionStart();
			Session::sessionSet('id', $user->get()['id']);
	}

	//login
	public function getLogin() {
        return $this->loadView('login');
	}

	public function postLogin($data)
	{
		$data['password'] = md5($data['password']);
		$user = new User();
		$result = $user->where($data);
		if($result == false) die("false");
		Session::sessionStart();
		Session::sessionSet('id', $result[0]->get()['id']);
		return(print_r(Session::sessionGet('id')));
	}

	public function getLogout()
	{
		Session::sessionStart();
		Session::sessionClose();
	}

	public function getPassReset() {
		return $this->loadView('passReset');		
	}

	public function postPassReset($data) {
		Session::sessionStart();
		$user = new User();
		$result = $user->where(['id' => Session::sessionGet('id')]);
		if($result[0]->get()['password'] == $data['old_password']) {
			$result[0]->update(['password' => $data['new_password']]);
		}

		return print_r($data['new_password']);		
	}

}
?>