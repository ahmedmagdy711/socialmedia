
CREATE TABLE IF NOT EXISTS `users_friends` (
	`id` INT NOT NULL ,
	`friend_id` INT NOT NULL,
	FOREIGN KEY (id) REFERENCES users (id),
	FOREIGN KEY (friend_id) REFERENCES users (id),
	PRIMARY KEY (id, friend_id)
);
