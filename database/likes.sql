
CREATE TABLE IF NOT EXISTS `likes` (
	`post_id` INT NOT NULL ,
	`user_id` INT NOT NULL,
	FOREIGN KEY (post_id) REFERENCES posts (id),
	FOREIGN KEY (user_id) REFERENCES users (id),
	PRIMARY KEY (post_id, user_id)
);
