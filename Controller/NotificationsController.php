<?php
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/core/Session.php');
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/core/Controller.php');

require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/Model/User.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/Model/Notification.php');
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/')  . '/Model/Group.php');

class NotificationsController extends Controller {
	public function __construct() {
	}

	public function index() {
		$user = new User();
		Session::sessionStart();
		$user->find(Session::sessionGet('id'));
		$notifications = $user->notification();
		$result = array();
		foreach ($notifications as $notification) {
			array_push($result, $notification->get());
		}
		return json_encode($result);
	}

	public function store($data) {
		// die('sss');
		$user = new User();
		$user->find($data['user_id']);

		$users_to_be_attached = array();
		if(isset($data['group_id'])) {
			$group = new Group();
			$group->find($data['group_id']);
			$users_to_be_attached = $group->user();
		} else {
			$users_to_be_attached = $user->friend();
		}

		unset($data['user_id']);
		$notification = new Notification();
		$notification->create($data);
		$notification->save();
		// $notification = $notification->where(['body' => $data['body']])[0];
		
		foreach ($users_to_be_attached as $friend) {
			$friend = $friend->get();
			$notification->many_to_many_attach('User', ['user_id' => $friend['id'],
			'notification_id' => $notification->get()['id']]);	
		}
		return $this->index();
	}

	public function show($data) {
		$notification = new Notification();
		$notification->find($data['id']);
		$notification = $notification->get();
		return json_encode($notification);
	}	
}
?>