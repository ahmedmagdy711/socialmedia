<?php

class Controller {

    protected $className, $methodName;
    protected $controllerfilePath;
    private $data, $viewName;
    public function __construct($className, $methodName, $data) 
    {
        $this->className  = $className;
        $this->methodName = $methodName;
        $this->data = $data;
        if($this->middleWare($this->className, $this->methodName, $this->data)) { 
            $this->data = $this->manageData(
                rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Controller/'.
                $className.'Controller.php',
                $this->className, $this->methodName, $this->data);
           //if(!empty($this->loadView($this->data['view']))) {echo $this->loadView($this->data['view']);}
        } else {
            header('Location: /');
            $this->data =  $this->loadView('mainPageNotLoged');
        }
    }

    private function middleWare($className, $methodName, $data) {
        $filePath = rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/MiddleWare/'.$className.'MiddleWare.php';
        $this->controllerfilePath = rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Controller/'.$className.'Controller.php';
        if(file_exists($this->controllerfilePath)) {
            require_once $filePath;
            $ack = new $className($this->controllerfilePath, $methodName, $data);
            return $ack->getAccess();
        } else {
            return false;
        }
    }

    private function manageData($filePath, $className, $methodName, $data) {
        require_once $filePath;
        $controllerclassName = $this->className . 'Controller';
        $controllerfile = new $controllerclassName();
        $data = $controllerfile->$methodName($data);
        return $data;
    }

    public function loadData(){
        return $this->data;
    }

    protected function loadView($viewName){
        $fp = fopen(rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Resources/views/'.
            $viewName.'.html', 'r');
        $page = fread($fp, filesize(rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Resources/views/'.
            $viewName.'.html'));
        fclose($fp);
        return $page;
    }

}

?>