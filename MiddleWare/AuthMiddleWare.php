<?php
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Session.php');

class Auth {
	private $methodName, $controllerfilePath;
	private $dataErrors = array();
	public $access;

	public function __construct($controllerfilePath, $methodName) {
		$this->methodName = $methodName;
		$this->controllerfilePath = $controllerfilePath;
		$this->access = True;
		Session::sessionStart();
		if(Session::sessionGet('id') != NULL && ($methodName == 'getRegister' 
			|| $methodName == 'postRegister' ||$methodName == 'getLogin'
			|| $methodName == 'postLogin')) {
			$this->access = False;
		} else if(Session::sessionGet('id') == NULL && (
			$methodName == 'getPassReset'
			|| $methodName == 'postPassReset')){
			$this->access = False;
		}
	}

	function methodExist() {
    	if(!method_exists(file($this->controllerfilePath), $this->methodName)) {
    		//return method not found
    	}
	}

	public function getAccess() {
		return $this->access;
	}


}
?>