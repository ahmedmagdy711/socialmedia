<?php

require rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/routes.php';
require rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') .'/core/Controller.php';
class Route{

	public function Request() {
		$methodData = (new routes()) -> getroutes()[$_SERVER['REQUEST_METHOD']];
		$Method = '$_'.$_SERVER['REQUEST_METHOD'];
		$currentData = explode('?',$_SERVER['REQUEST_URI']);
		$url =  $currentData[0];
		$urlData = $this->getUrlData($_SERVER['REQUEST_METHOD']);
		$className = explode(':', $methodData[$url])[0];
		$methodName = explode(':', $methodData[$url])[1];
		$incomingData = (new Controller($className, $methodName, $urlData)) -> loadData();
		echo $incomingData;
		// if(empty($incomingData)){
		// 	$view = $incomingData['view'];
		// 	$data = $incomingData['data'];
		// }
	}

	private function getUrlData($requestMethod) {
		if($requestMethod === 'GET') {
			return $_GET;
		} else if ($requestMethod === 'POST') {
			return $_POST;
		}
	}
}
$con = new Route(); $con->Request();
?>