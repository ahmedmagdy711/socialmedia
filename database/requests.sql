
CREATE TABLE IF NOT EXISTS `users_requests` (
	`id` INT NOT NULL ,
	`request_id` INT NOT NULL,
	FOREIGN KEY (id) REFERENCES users (id),
	FOREIGN KEY (request_id) REFERENCES users (id),
	PRIMARY KEY (id, request_id)
);