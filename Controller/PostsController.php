<?php
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Session.php');
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Controller.php');

require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/Post.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/User.php');
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/Group.php');
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/Comment.php');


class PostsController extends Controller {
	public function __construct() {
	}

	public function index() {
		$user = new User();
		Session::sessionStart();
		$posts = array();
		$users = $user->all();
		$result = array();
		// die(var_dump($users));
		foreach ($users as $suser) {
			$uposts = $this->userpost(['id' => $suser->get()['id']]);
		 	$result = array_merge($result, json_decode($uposts));
			// die(var_dump($result));
		 	// ($result, json_decode($uposts));
		}
		die( json_encode($result) );
		
	}

	public function create() {
		return $this->loadView('createPost');
	}

	public function store($data) {
		Session::sessionStart();
		$data['user_id'] = Session::sessionGet('id');
		$haveImage = isset($data['haveimage']);
		$img ;
		if($haveImage)
		{
		$img = $data['haveimage'];
		unset($data['haveimage']);
			
		}
		// die(($haveImage == 'true') ? 't' : 'f');
		$post = new Post();
		$post->create($data);
		$post->save();
		if($haveImage) {
			chmod($img.'.jpg', 0777);
			$uploaddir = '/var/www/html/facebook/socialmedia/public/';
			copy($uploaddir.$img.'.jpg', $uploaddir.'post-'.$post->get()['id'].'.jpg');
			$post->update_img(['img' => '/post-'.$post->get()['id'].'.jpg']);
		}
		$user = new User();
		$user->find($data['user_id']);
		$user = $user->get();
		/*die($user['fname']);
		die (json_encode(print_r($data)));*/
		$group_name = '';
		if(isset($data['group_id'])) {
			$group = new Group();
			$group->find($data['group_id']);
			$group_name = $group->get()['name'];
		} else {
			$group_name = 'in '.(($user['gender'] == 1) ? 'his':'her').' time line';
		}
		$message = $user['fname'] . ' ' . $user['lname'] . ' has made a post ' . $group_name;
		$data = array('body' => $message, 'user_id' => $data['user_id'], 
			          'group_id' => isset($data['group_id']) ? $data['group_id'] : NULL  );
		// die(print_r($data));
		new Controller('Notifications', 'store', $data);

		// die(print_r($data));
		return $this->index();
	}

	public function show($data) {
		$post = new Post();
		$post->find($data['id']);
		$post =$post->get();
		return json_encode($post);
	}

	public function edit() {
		// return $this->loadView('edit');
	}

	public function update($data) {
		$post = new Post();
		$post->find($data['id']);
		$post->update($data);
		return $this->loadView('index');
	}

	public function userpost($data) {
		$user = new User();
		$user->find($data['id']);
		$isfriend = $this->inFriendList($data['id']);
		$posts = $user->post();
		$result = array();
		$results = array();
		foreach ($posts as $post) {
			if($data['id'] != $this->current_user_id())
				if(!$isfriend) {
					if($post->get()['public'] == 'private') {
						continue;
					}
				}
			$liked = "fa fa-thumbs-o-up fa-2x";
			$result = $post->get();
			$result['user'] = array('fname' => $post->user()->get()['fname'],
				'lname' => $post->user()->get()['lname'] ,
				'profile_image' => $post->user()->get()['profile_image']);
			$comments = array();
			for ($i = 0 ; $i < count($post->comment()) ; $i++) {
				$user = $post->comment()[$i]->user();
				$comments[$i] =  $post->comment()[$i]->get();
				$comments[$i]['name'] = $user->get()['fname']." ".$user->get()['lname'];
			}
			$result['comments'] = $comments;
			$likes = array();
			for ($i = 0 ; $i < count($post->like()) ; $i++) {
				$user = new User();
				$user->find($post->like()[$i]['user_id']);
				if(Session::sessionGet('id') == $post->like()[$i]['user_id']) {
					$liked = "fa fa-thumbs-up fa-2x";
				}
				$likes[$i] =  ['id' => $user->get()['id'] ,
				 'name' => $user->get()['fname'] ] ;
			}
			$result['liked'] = $liked;
			$result['likes'] = $likes;
			array_push($results, $result);
		}
		 	return(json_encode($results));
	}

	public function destroy($data) {
		$post = new Post();
		$post->find($data['id']);
		if($post->get()) $post->remove(); else echo 'there is no post with that id';
		return $this->loadView('index');
	}

	public function like($data) {
		Session::sessionStart();
		$post = new Post();
		$post->find($data['id']);
		$post->insert_into_table('likes', ['post_id' => $data['id'],
		 'user_id' => Session::sessionGet('id')]);
		return $this->index();
	}

	public function comment($data) {
		Session::sessionStart();
		$comment = new comment();
		$data['user_id'] = Session::sessionGet('id');
		$comment->create($data);
		$comment->save();
		$user = $comment->user();
		$result = $comment->get();
		$result['name'] = $user->get()['fname']." ".$user->get()['lname'];
		return json_encode($result);
	}

	private function current_user_id() {
		Session::sessionStart();
		return Session::sessionGet('id');
	}

	private function inFriendList ($id) {
		$user = new User();
		$user->find($id);
		$friends = $user->friend();
		$current_id = $this->current_user_id();
		$found = false;
		foreach ($friends as $friend) {
			$friend = $friend->get();
			if($friend['id'] == $current_id ) {
				$found = true;
				break;
			} else {
				$found = false;
			}
		}

		return $found;
	}
}
?>