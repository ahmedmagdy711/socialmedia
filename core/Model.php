<?php
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/'). '/database/Commands.php');

class Model extends Commands 
{
    public $db;

    public $table_entities = array();


    public function __construct(){
        $this->db = new Commands();
    }

    public function loadEntities() {
        $fp = fopen(rtrim($_SERVER['DOCUMENT_ROOT'], 'public/'). '/database/'.strtolower(get_class($this)."s").'.sql', 'r');
        $db_schema = fread($fp, filesize(rtrim($_SERVER['DOCUMENT_ROOT'], 'public/'). '/database/'.strtolower(get_class($this)."s").'.sql'  ));
        for ($i=3; $i < count(explode("`", $db_schema)); $i+=2) { 
            $this->table_entities[explode("`", $db_schema)[$i]]="";
        }
        fclose($fp);
    }

    public function all() {
        $query_result = $this->db->db_select_all_table(strtolower(get_class($this)."s"),"");
        $result = array();
        while($row = mysqli_fetch_assoc($query_result)) {
            require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/Model/" . get_class($this).'.php';
            $class = get_class($this);
            $return_role = new $class($this);
            $return_role->create($row);
            array_push($result, $return_role);
        }
        return $result;
    }
    public function insert_into_table($table , $data) {
        $fields = "";
        foreach ($data as $key => $value) {
            $fields = $fields."$key,";
        }
        $fields = rtrim($fields, ",");
        $values = "";
        foreach ($data as $key => $value) {
            $values = $values."'$value',";
        }
        $values = rtrim($values, ",");
        $this->db->db_insert($table, $fields, $values);
    }

    public function find($key) {
        $result =  $this->db->db_select_all_by_id(strtolower(get_class($this)."s"), "$key");
        if($result == NULL) return;
        foreach ($result as $key => $value) {
            $this->table_entities[$key] = $value;
        }
    }

    public function where($condtion) {
        $fields = "";
        foreach ($condtion as $key => $value) {
            $fields = $fields.$key." = '$value' AND ";
        }
        $fields = rtrim($fields, "AND ");
        $result = array();
        $query_result = $this->db->db_select_all_where(strtolower(get_class($this)."s"), $fields);
        if($query_result->num_rows == 0) {
            return false;
        }
        while($row = mysqli_fetch_assoc($query_result)) {
            // die(rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
            //  "/facebook/socialmedia/Model/User" . get_class($this).'.php');
            require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/Model/" . get_class($this).'.php';
            $class = get_class($this);
            $return_role = new $class($this);
            $return_role->create($row);
            array_push($result, $return_role);
        }
        return $result;

    }

    public function create($input) {
        foreach ($input as $key => $value) {
            $this->table_entities[$key] = $value;
        }
    }

    public function save() {
        $fields = "";
        foreach ($this->table_entities as $key => $value) {
            if($value != NULL) {
                $fields = $fields.$key.",";
            }
        }
        $fields = rtrim($fields, ",");
        $values = "";
        foreach ($this->table_entities as $key => $value) {
            if($value != NULL) {
                $values = $values."'".$value."'".", ";
            } 
        }
        $values = rtrim($values, ", ");
        $this->table_entities['id'] = $this->db->db_insert(strtolower(get_class($this)."s"), $fields, $values);
    }

    public function get() {
        return($this->table_entities);
    }

    public function update($data) {
        foreach ($data as $key => $value) {
            $this->table_entities[$key] = $value;
        }
        $fields = "";
        foreach ($this->table_entities as $key => $value) {
            if($key != 'id')
            $fields = $fields.$key." = '$value',";
        }
        $fields = rtrim($fields, ",");
        $this->db->db_update(strtolower(get_class($this)."s"), $fields,
         $this->table_entities['id']);
    }

    public function update_img($data) {
        $fields = "img = '" . $data['img'] ."'";
        $this->db->db_update(strtolower(get_class($this)."s"), $fields,
         $this->table_entities['id']);
    }

    public function remove() {
        $this->db->db_delete(strtolower(get_class($this)."s"), $this->table_entities['id']);
    }

    public function attach($role, $key) {
        $this->db->db_insert_attach(strtolower(get_class($this)."s")."_".$role."s",
         $this->table_entities['id'], $key);
    }
    public function attach_inverse($role, $key) {
        $this->db->db_insert_attach(strtolower(get_class($this)."s")."_".$role."s",
         $key, $this->table_entities['id']);
    }

    public function dettach($role, $value) {
        $this->db->db_delete_dettach(strtolower(get_class($this)."s")."_".$role."s",
          $this->table_entities['id'], $role, $value);
    }

    public function belongs_to_many($role) {
        $result = array();
        $query_result = $this->db->db_select_all_by_id_belongs(strtolower(get_class($this)."s"),
            strtolower(get_class($this)."s")."_".$role."s",
         $this->table_entities['id']);
        while($row = mysqli_fetch_assoc($query_result)) {
            require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/Model/" . get_class($this).'.php';
            $class = get_class($this);
            $return_role = new $class($this);
            $return_role->create($row);
            array_push($result, $return_role);
        }
        return $result;
    }

    public function belongs_to_many_to_many($role) {
        $stable_file = rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/database/" .
              strtolower(get_class($this)."s")."_".strtolower($role)."s".'.sql';
        if(!file_exists($stable_file)) {
            $stable_file = rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/database/" .
              strtolower($role)."s"."_".strtolower(get_class($this)."s").'.sql';

            $stable_file =  strtolower($role)."s"."_".strtolower(get_class($this)."s");
        } else {
            $stable_file = strtolower(get_class($this)."s")."_".strtolower($role)."s";
        }
        $result = array();
        $query_result = $this->db->db_select_all_by_id_belongs_many(strtolower($role."s"),
            $stable_file,
         $this->table_entities['id']);
        while($row = mysqli_fetch_assoc($query_result)) {
            require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/Model/" . get_class($this).'.php';
            $class = get_class($this);
            $return_role = new $class($this);
            $return_role->create($row);
            array_push($result, $return_role);
        }
        return $result;
    }

    public function belongs_to($role) {
        require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/' ).
             "/Model/" . $role.'.php';
        $return_role = new $role();
        $return_role->create($this->db->db_select_all_by_id(strtolower(($role)."s"),
         $this->table_entities[strtolower($role.'_id')]));
        return $return_role;
    }

    public function hasMany($role) {
        $result = array();
        $query_result = $this->db->db_select_all_by_id_many(strtolower(($role)."s"), 
            strtolower(get_class($this)."_id"),
         $this->table_entities['id']);
        if($query_result != false)
        while($row = mysqli_fetch_assoc($query_result)) {
            if(!file_exists(rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/Model/" . $role.'.php')) {
                array_push($result, $row);
                continue;
            }
            require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/Model/" . $role.'.php';
            $return_role = new $role();
            $return_role->create($row);
            array_push($result, $return_role);
        }
        return $result;
    }

    public function many_to_many_attach($role, $data) {
        $table_file = rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/database/" .
              strtolower(get_class($this)."s")."_".strtolower($role)."s".'.sql';
        if(!file_exists($table_file)) {
            $table_name = rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/database/" .
              strtolower($role)."s"."_".strtolower(get_class($this)."s").'.sql';

            $table_name =  strtolower($role)."s"."_".strtolower(get_class($this)."s");
        } else {
            $table_name = strtolower(get_class($this)."s")."_".strtolower($role)."s";
        }
        $dataToInsert = "(";
        foreach ($data as $key => $value) {
            $dataToInsert = $dataToInsert . $key.", "; 
        }

        $dataToInsert = rtrim($dataToInsert, ", ");
        $dataToInsert = $dataToInsert. ") VALUES (";
        foreach ($data as $key => $value) {
            $dataToInsert = $dataToInsert . "'". $value . "', "; 
        }
        $dataToInsert = rtrim($dataToInsert, ", ");
        $dataToInsert = $dataToInsert.")";
        $this->db->db_insert_attach_many($table_name, $dataToInsert);
    }

    public function many_to_many($role) {
        $table_file = rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/database/" .
              strtolower(get_class($this)."s")."_".strtolower($role)."s".'.sql';
        if(!file_exists($table_file)) {
            $table_name = rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/database/" .
              strtolower($role)."s"."_".strtolower(get_class($this)."s").'.sql';

            $table_name =  strtolower($role)."s"."_".strtolower(get_class($this)."s");
        } else {
            $table_name = get_class($this)."_".strtolower($role)."s";
        }
        $dataToInsert = strtolower(get_class($this)). "_id = " . $this->table_entities['id'];

        $query_result = $this->db->db_select_all_where($table_name, $dataToInsert);
        while($row = mysqli_fetch_assoc($query_result)) {
            require_once rtrim($_SERVER['DOCUMENT_ROOT'], 'public/').
             "/Model/" . ($role).'.php';
            $return_role = new $role();
            $return_role->create($row);
            die($role);
            array_push($result, $return_role);
        }
        return $result;
    }
}
?>