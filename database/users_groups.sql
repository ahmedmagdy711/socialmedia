CREATE TABLE IF NOT EXISTS `users_groups` (
	`user_id` INT,
	`group_id` INT,
	`join_Date` TIMESTAMP,
	PRIMARY KEY (user_id, group_id),
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS `users_notifications`(
	`notification_id` INT,
	`user_id` INT,
	PRIMARY KEY(notification_id, user_id),
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (notification_id) REFERENCES notifications (id) ON DELETE CASCADE
);