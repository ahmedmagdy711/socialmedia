CREATE TABLE IF NOT EXISTS `users_notifications`(
	`notification_id` INT,
	`user_id` INT,
	PRIMARY KEY(notification_id, user_id),
	FOREIGN KEY (user_id) REFERENCES users (id),
	FOREIGN KEY (notification_id) REFERENCES notifications (id) ON DELETE CASCADE
);