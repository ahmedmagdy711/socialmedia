<?php
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Session.php');
require_once (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/core/Controller.php');

require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/Post.php');
require (rtrim($_SERVER['DOCUMENT_ROOT'], 'public/') . '/Model/User.php');

class UsersController extends Controller {
	public function __construct() {
	}

	public function me() {
		$user = new User();
		Session::sessionStart();
		$user->find(Session::sessionGet('id'));
		$user = $user->get();
		unset($user['password']);
		return json_encode($user);
	}

	public function show($data) {
		$user = new User();
		$user->find($data['id']);
		//die(print_r(json_encode($user->get())));
		$user = $user->get();
		unset($user['password']);
		if ($this->inFriendList($data['id']) ||
		 $this->current_user_id() == $data['id']) {
			return json_encode($user);
		} else {
			unset($user['birthdate']);
			return json_encode($user);
		}
	}

	public function edit() {
	}

	public function update($data) {
		$user = new User();
		$user->find($this->current_user_id());
		$user->update($data);
		return $this->me();
	}

	public function add($data) {
		$user = new User();
		$user->find($this->current_user_id());
		if($this->inFriendList($data['id'])) {
			die('here');

		} else if($user->get()['id'] != $data['id']){
			$user->attach_inverse('request', $data['id']);
		}
	}

	public function accept($data) {
		$cuser = new User();
		$cuser->find($this->current_user_id());
		if($this->inRequestList($data['id'])) {
			$cuser->attach('friend', $data['id']);
			$user = new User();
			$user->find($data['id']);
			$user->attach('friend', $this->current_user_id());
			$cuser->dettach('request', $data['id']);
		}
	}

	public function friends($data) {
		$user = new User();
		$user->find($this->current_user_id());
		$result = array();
		foreach ($user->friend() as $friend) {
			array_push($result, $friend->get());
		}
		return json_encode($result);
	}

	public function request($data) {
		$user = new User();
		$user->find($this->current_user_id());
		$result = array();
		foreach ($user->request() as $user) {
			array_push($result, $user->get());
		}
		return json_encode($result);
	}

	private function current_user_id() {
		Session::sessionStart();
		return Session::sessionGet('id');
	}

	private function inFriendList ($id) {
		$user = new User();
		$user->find($id);
		$friends = $user->friend();
		$current_id = $this->current_user_id();
		$found = false;
		foreach ($friends as $friend) {
			$friend = $friend->get();
			if($friend['id'] == $current_id ) {
				$found = true;
				break;
			} else {
				$found = false;
			}
		}

		return $found;
	}

	private function inRequestList ($id) {
		$user = new User();
		$user->find($this->current_user_id());
		$friends = $user->request();
		$found = false;
		foreach ($friends as $friend) {
			$friend = $friend->get();
			if($id == $friend['id'] || $id = $current_id ) {
				$found = true;
				break;
			} else {
				$found = false;
			}
		}

		return $found;
	}
}
?>